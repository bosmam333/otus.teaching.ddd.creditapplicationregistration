﻿namespace Otus.Teaching.Ddd.Contract
{
    public enum CreditApplicationStatus
    {
        New = 1,
        InWork = 2,
        Finished = 3
    }
}