﻿namespace Otus.Teaching.Ddd.Cbs.CreditApplicationAggregator.Host
{
    public class IntegrationEndpointSettings
    {
        public string MessageBrokerEndpoint { get; set; }
    }
}