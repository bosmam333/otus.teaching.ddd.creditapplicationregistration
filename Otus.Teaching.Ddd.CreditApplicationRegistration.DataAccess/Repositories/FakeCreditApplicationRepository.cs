﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Repositories;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.DataAccess.Repositories
{
    public class FakeCreditApplicationRepository
        : ICreditApplicationRepository
    {
        public Task AddAsync(CreditApplication entity)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Guid entityId)
        {
            throw new NotImplementedException();
        }

        public Task SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CreditApplication>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<CreditApplication> GetByIdAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(CreditApplication entity)
        {
            throw new NotImplementedException();
        }
    }
}
