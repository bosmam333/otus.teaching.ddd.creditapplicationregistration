﻿using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Ddd.Contract;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Gateways;
using CreditApplicationStatus = Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg.CreditApplicationStatus;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Integration.Producers
{
    public class CreditApplicationPipelineGateway
        : ICreditApplicationPipelineGateway
    {
        private readonly ISendEndpointProvider _sendEndpointProvider;
        private readonly ILogger<CreditApplicationPipelineGateway> _logger;

        public CreditApplicationPipelineGateway(ISendEndpointProvider sendEndpointProvider, ILogger<CreditApplicationPipelineGateway> logger)
        {
            _sendEndpointProvider = sendEndpointProvider;
            _logger = logger;
        }

        public async Task RegisterNewCreditApplication(CreditApplicationImportDto application)
        {
            _logger.LogInformation($"Отправляем команду на создание кредитной заявки с Id {application.Id} в шину...");
            
            await _sendEndpointProvider.Send<CreateNewCreditApplicationContract>(new CreditApplicationImportDto()
            {
                
            });
            
            _logger.LogInformation($"Отправили команду на создание кредитной заявки с Id {application.Id} в шину");
        }

        public async Task<CreditApplicationStatus> CheckOrderStatus(CreditApplication application)
        {
            return await Task.FromResult(CreditApplicationStatus.New);
        }
    }
}