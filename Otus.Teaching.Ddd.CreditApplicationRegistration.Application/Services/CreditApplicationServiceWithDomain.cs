﻿using System;
using System.Linq;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Repositories;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Services;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Gateways;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg;
using System.Threading.Tasks;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Application.Services
{
    public class CreditApplicationServiceWithDomain : ICreditApplicationService
    {
        private readonly ICreditApplicationRepository _creditApplicationRepository;
        private readonly ICreditApplicationPipelineGateway _creditPipelineGateway;
        private readonly ICustomerCreditHistoryGateway _customerCreditHistoryGateway;

        public CreditApplicationServiceWithDomain(ICreditApplicationRepository creditApplicationRepository, 
            ICreditApplicationPipelineGateway createNewCreditProducer, ICustomerCreditHistoryGateway customerCreditHistoryGateway)
        {
            _creditApplicationRepository = creditApplicationRepository;
            _creditPipelineGateway = createNewCreditProducer;
            _customerCreditHistoryGateway = customerCreditHistoryGateway;
        }


        public async Task<CreditApplicationsForListDto> GetCreditApplicationsForListAsync()
        {
            var customers = await _creditApplicationRepository.GetAllAsync();

            return new CreditApplicationsForListDto()
            {
                Items = customers.Select(x => new CreditApplicationsForListItemDto()
                {
                    Id = x.Id,
                    Channel = x.Customer.Channel.ToString(),
                    Status = x.Status.ToString(),
                    Sum = x.CreditTerms.Sum,
                    CreatedDate = x.CreatedDate,
                    CustomerId = x.Customer.Id
                }).ToList()
            };
        }

        public async Task<CreditApplicationDto> GetCreditApplicationAsync(Guid id)
        {
            var application = await _creditApplicationRepository.GetByIdAsync(id);

            if(application == null)
                throw new Exception("Application not found");

            return new CreditApplicationDto()
            {
                Id = application.Id,
                Channel = application.Customer.Channel,
                Status = application.Status,
                Sum = application.CreditTerms.Sum,
                CreatedDate = application.CreatedDate,
                CustomerId = application.Customer.Id
            };
        }

        public async Task<CreditApplicationReadyToSendDto> CreateCreditApplicationAsync(CreateCreditApplicationDto creditApplicationDto)
        {
            if (creditApplicationDto == null)
                throw new ArgumentNullException(nameof(creditApplicationDto));

            //Создаем агрегат
            var creditApplication = new CreditApplication(
                new Customer(
                    new FullName(creditApplicationDto.FirstName, creditApplicationDto.LastName),
                    new Contact(new Email(creditApplicationDto.Email),
                    new Phone(creditApplicationDto.Phone)),
                    creditApplicationDto.Channel),
                new CreditTerms(creditApplicationDto.Term, creditApplicationDto.DesiredRate, creditApplicationDto.Sum),
                new Employee(
                    new FullName(creditApplicationDto.AuthorFirstName, creditApplicationDto.AuthorLastName), 
                    new Email(creditApplicationDto.Email), 
                    creditApplicationDto.AuthorLogin), 
                Guid.NewGuid());

            var customerHasOverduePayments =  await _customerCreditHistoryGateway.CheckThatCustomerHaveOverdueCreditsAsync(creditApplication.Customer);
            //Считаем возможную ставку
            creditApplication.CalculatePossibleRate(customerHasOverduePayments);

            await _creditApplicationRepository.AddAsync(creditApplication);

            await _creditPipelineGateway.RegisterNewCreditApplication(new CreditApplicationImportDto()
            {
                //Заполняем данные заявки
            });           

            return new CreditApplicationReadyToSendDto()
            {
                Id = creditApplication.Id
            };
        }

        public async Task RegisterCreditApplicationStatusAsync(Guid id)
        {
            var application = await _creditApplicationRepository.GetByIdAsync(id);

            var currentStatus = await _creditPipelineGateway.CheckOrderStatus(application);
            
            if(currentStatus == CreditApplicationStatus.Registered)
            {
                application.Register();
            }

            await _creditApplicationRepository.UpdateAsync(application);
        }
    }
}
