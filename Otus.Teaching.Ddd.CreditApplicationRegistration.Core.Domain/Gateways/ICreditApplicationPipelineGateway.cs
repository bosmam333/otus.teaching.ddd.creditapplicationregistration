﻿using System.Threading.Tasks;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Gateways
{
    public interface ICreditApplicationPipelineGateway
    {
        Task RegisterNewCreditApplication(CreditApplicationImportDto application);

        Task<CreditApplicationStatus> CheckOrderStatus(CreditApplication application);
    }
}