﻿using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Gateways
{
    public interface ICustomerCreditHistoryGateway
    {
        Task<bool> CheckThatCustomerHaveOverdueCreditsAsync(Customer customer);
    }
}
