using System;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork
{
    public interface ICurrentDateTimeProvider
    {
        DateTime GetNow();

        DateTime GetToday();
    }
}