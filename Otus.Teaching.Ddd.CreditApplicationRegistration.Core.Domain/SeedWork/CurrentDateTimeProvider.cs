using System;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork
{
    public class CurrentDateTimeProvider
        : ICurrentDateTimeProvider
    {
        public DateTime GetNow()
        {
            return  DateTime.Now;
            ;
        }

        public DateTime GetToday()
        { 
            return DateTime.Today;
        }
    }
}