using System;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork
{
    public class DomainException
        : Exception
    {
        public DomainException()
        {
            
        }

        public DomainException(string message)
            :base(message)
        {
            
        }
    }
}