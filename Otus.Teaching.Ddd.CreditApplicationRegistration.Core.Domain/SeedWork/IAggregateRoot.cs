﻿namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork
{
    /// <summary>
    /// Marker of aggregate's root
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
