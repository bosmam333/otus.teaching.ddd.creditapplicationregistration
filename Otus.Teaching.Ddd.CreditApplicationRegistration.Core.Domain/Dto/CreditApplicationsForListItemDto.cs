﻿﻿ using System;

  namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto
{
    public class CreditApplicationsForListItemDto
    {
        public Guid Id { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public string Channel { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public string Status { get; set; }
        
        public decimal Sum { get; set; }

    }
}
