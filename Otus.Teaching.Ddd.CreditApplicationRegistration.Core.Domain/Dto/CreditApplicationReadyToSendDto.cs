﻿﻿
using System;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto
{
    public class CreditApplicationReadyToSendDto
    {
        public Guid Id { get; set; }
    }
}
