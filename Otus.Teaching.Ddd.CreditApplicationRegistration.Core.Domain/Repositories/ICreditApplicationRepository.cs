﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
    using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Repositories
{
    public interface ICreditApplicationRepository
    {
        Task<IEnumerable<CreditApplication>> GetAllAsync();

        Task<CreditApplication> GetByIdAsync(Guid id);

        Task AddAsync(CreditApplication entity);
        
        Task UpdateAsync(CreditApplication entity);

        Task DeleteAsync(Guid entityId);

        Task SaveChangesAsync();
    }
}