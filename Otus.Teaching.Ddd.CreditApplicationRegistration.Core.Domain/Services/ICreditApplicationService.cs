﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Dto;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Services
{
    public interface ICreditApplicationService
    {
        Task<CreditApplicationsForListDto> GetCreditApplicationsForListAsync();
        
        Task<CreditApplicationDto> GetCreditApplicationAsync(Guid id);
        
        Task<CreditApplicationReadyToSendDto> CreateCreditApplicationAsync(CreateCreditApplicationDto creditApplicationDto);
    }
}
