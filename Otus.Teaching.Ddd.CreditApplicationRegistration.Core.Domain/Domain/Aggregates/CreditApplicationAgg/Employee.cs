﻿using System;
using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg
{
    public class Employee
        : IValueObject
    {
        public FullName FullName { get; private set; }

        public string Login { get; private set; }

        public Email Email { get; private set; }
        
        public Employee(FullName fullName, Email email, string login)
        {
            FullName = fullName;
            Email = email;
            Login = login;
        }

        protected Employee()
        {
            
        }
    }
}