﻿using Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg
{
    /// <summary>
    /// Условия кредита
    /// </summary>
    public class CreditTerms
        : IValueObject
    {
        /// <summary>
        /// Сумма кредита
        /// </summary>
        public decimal Sum { get; set; }

        /// <summary>
        /// Желаяем ставка в процентах
        /// </summary>
        public decimal DesiredRateInPercents { get; set; }

        /// <summary>
        /// Вероятная ставка
        /// </summary>
        public decimal PossibleRateInPercents { get; set; }

        /// <summary>
        /// Срок кредита
        /// </summary>
        public DateTime Term { get; set; }

        public CreditTerms(DateTime term, int desiredRate, decimal sum)
        {
            Sum = sum;
            DesiredRateInPercents = desiredRate;
            Term = term;
        }

        public void SetPossibleRate(decimal calculatedRate)
        {
            PossibleRateInPercents = calculatedRate;
        }
    }
}
