﻿namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.Aggregates.CreditApplicationAgg
{
    public enum CreditApplicationStatus
    {
        /// <summary>
        /// Новая
        /// </summary>
        New = 1,

        /// <summary>
        /// Зарегистрирована 
        /// </summary>
        Registered = 2,

        /// <summary>
        /// На рассмотрении
        /// </summary>
        UnderConsideration = 3,

        /// <summary>
        /// Завершена
        /// </summary>
        Finished = 4,

        /// <summary>
        /// Отказано
        /// </summary>
        Denied = 5
    }
}