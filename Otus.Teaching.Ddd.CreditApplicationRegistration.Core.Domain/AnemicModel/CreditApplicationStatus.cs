﻿namespace Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain.AnemicModel
{
    public enum CreditApplicationStatus
    {
        New = 1,
        InWork = 2,
        Finished = 3
    }
}