## Демо для занятия "Domain Driven Design"

Проект с демонастрацией некоторых DDD-шаблонов для домена регистрации заявки 
на кредит

Описание проектов

#### Otus.Teaching.Ddd.CreditApplicationRegistration.Host

Web Api

#### Otus.Teaching.Ddd.CreditApplicationRegistration.Application

Основная логика приложения

#### Otus.Teaching.Ddd.CreditApplicationRegistration.Core.Domain

Модель предметной области

#### Otus.Teaching.Ddd.CreditApplicationRegistration.DataAccess

Слой доступа к данным

#### Otus.Teaching.Ddd.CreditApplicationRegistration.Integration

Слой интеграции
